<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php if ( astra_page_layout() == 'left-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

	<div id="primary" <?php astra_primary_class(); ?>>

		<section class="error-404 not-found">
			<div class="ast-404-layout-1">
				<header class="page-header">
					<h1 class="page-title">This content has been made Inkognito and taken into the underground.</h1>
				</header><!-- .page-header -->
				<div class="page-content">
					<div class="page-sub-title"> If you're looking to buy a ticket you'll need to login using the special password we sent you or use the link below to apply for an Inkognito login</div>
					<div class="ast-404-search flex justify-content-center flex-wrap">
						<div class="login-box flex flex-wrap">
							<?php my_wp_login_form() ?>
						</div>
					</div>
				</div><!-- .page-content -->
			</div>
		</section>

	</div><!-- #primary -->

<?php if ( astra_page_layout() == 'right-sidebar' ) : ?>

	<?php get_sidebar(); ?>

<?php endif ?>

<?php get_footer(); ?>

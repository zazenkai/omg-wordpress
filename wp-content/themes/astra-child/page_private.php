<?php
/**
 * The Template for displaying news page.
 *
 * @package WordPress
 * @subpackage pBone
 * Template name: Private Events Page
 */?>
<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header(); 
?>
<?php // custom loop - posts on a page with paging
$paged = get_query_var('paged') ? get_query_var('paged') : 1;
$args = array('posts_per_page' => 20, 'paged' => $paged, 'cat' => '41');

$temp = $wp_query;
$wp_query= null;

$wp_query = new WP_Query();
$wp_query->query($args); ?>

<?php if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
<div class="tribe-common tribe-events tribe-events-view tribe-events-view--list tribe-common--breakpoint-xsmall tribe-common--breakpoint-medium tribe-common--breakpoint-full">
<article class="tribe-events-calendar-list__event tribe-common-g-row tribe-common-g-row--gutters post-365 tribe_events type-tribe_events status-publish has-post-thumbnail hentry tribe_events_cat-public-events cat_public-events ast-col-sm-12 ast-article-post">
	<?php if(has_post_thumbnail()): ?>
	<div class="tribe-events-calendar-list__event-featured-image-wrapper tribe-common-g-col">
		<a href="<?php the_permalink(); ?>" class="tribe-events-calendar-list__event-featured-image-link">
			<?php
				if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
					the_post_thumbnail( 'full', array( 'class'  => 'tribe-events-calendar-list__event-featured-image' ) ); // show featured image
				} 
			?>
		</a>
	</div>
	<div class="content tribe-event-content">
	<?php else: ?>
	<div class="content" style="width:auto;">
	<?php endif; ?>
		<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php the_content(); ?>
		<?php if(get_field('buy_now_link')): ?>
			
		<a href="<?php the_field('buy_now_link'); ?>" class="button button-primary">
			Get Tickets		</a>
		<?php endif; ?>
	</div>
</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
	There are currently no private events.
<?php endif; ?>
</article>
</div>
<?php wp_reset_query(); ?>

<?php get_footer(); ?>
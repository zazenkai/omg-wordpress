<?php

/**

 * The template for displaying the footer.

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package Astra

 * @since 1.0.0

 */



if ( ! defined( 'ABSPATH' ) ) {

	exit; // Exit if accessed directly.

}



?>

<?php astra_content_bottom(); ?>

	</div> <!-- ast-container -->

	</div><!-- #content -->

<?php 

	astra_content_after();

		

	astra_footer_before();

		

	astra_footer();

		

	astra_footer_after(); 

?>

	</div><!-- #page -->

<?php 

	astra_body_bottom();    

	wp_footer(); 

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/PreloadJS/1.0.1/preloadjs.js"></script>
<script id="rendered-js" >
var preloaded = document.querySelector('.preloaded');
var queue = new createjs.LoadQueue(true, null, true);

queue.on('fileload', handleLoad, this);

queue.on('progress', handleProgress, this);

queue.on('complete', handleComplete, this);

queue.loadManifest([
{
  id: 'sf',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/omg-slideshow/20210418_140654.webp' },

{
  id: 'hk',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/omg-slideshow/20210418_144143.webp' },

{
  id: 'lon',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/omg-slideshow/Langhuset-arrangement-2-scaled-1.webp' },

{
  id: 'nyc',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/omg-slideshow/20210418_143231.webp' },

{
  id: 'tok',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/omg-slideshow/20210418_153954.webp' }]);



function handleProgress(event) {
  console.log(event.progress);
}

function handleLoad(event) {
  // console.log(event);
  var img = new Image();
  // img.crossOrigin = 'Anonymous';
  img.src = event.item.src;
  preloaded.appendChild(img);
  // var img = queue.getResult('image',true);
  // preloaded.appendChild(event.result);
}

function handleComplete() {
  document.querySelector('.main-container').classList.add('loaded');
  console.log('done');
}
//# sourceURL=pen.js
	
	
function refreshDiv(){
 var container = document.getElementById("main-container");
 var content = container.innerHTML;
//alert(content);
container.innerHTML= content;
}

setInterval(refreshDiv, 30000);

	
//function myFunction() {
//  var i = 0;
//  do {
//    i++;
//
//  }
//  while (i < 10);
//  
//}
//	
//myFunction();
</script>
	</body>

</html>


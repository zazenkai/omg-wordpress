<?php
/**
 * The Template for displaying the festival page.
 *
 * @package WordPress
 * @subpackage pBone
 * Template name: Festival Page
 */?>
<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header(); 
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="inner-consciousness">
		<?php the_content(); ?>
	</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>


<?php wp_reset_query(); ?>

<?php get_footer(); ?>

<script>
(function () {
  const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

  let birthday = "Jul 09, 2021 14:00:00",
      countDown = new Date(birthday).getTime(),
      x = setInterval(function() {    

        let now = new Date().getTime(),
            distance = countDown - now;

        document.getElementById("days").innerText = Math.floor(distance / (day)),
          document.getElementById("hours").innerText = Math.floor((distance % (day)) / (hour)),
          document.getElementById("minutes").innerText = Math.floor((distance % (hour)) / (minute)),
          document.getElementById("seconds").innerText = Math.floor((distance % (minute)) / second);

        //do something later when date is reached
        if (distance < 0) {
          let headline = document.getElementById("headline"),
              countdown = document.getElementById("countdown"),
              content = document.getElementById("content");

          headline.innerText = "The countdown is over";
          countdown.style.display = "none";
          content.style.display = "block";

          clearInterval(x);
        }
        //seconds
      }, 0)
  }());
</script>

<script id="rendered-js" >
var preloaded = document.querySelector('.preloaded');
var queue = new createjs.LoadQueue(true, null, true);

queue.on('fileload', handleLoad, this);

queue.on('progress', handleProgress, this);

queue.on('complete', handleComplete, this);

queue.loadManifest([
{
  id: 'sf',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/04/20210418_140654.jpg' },

{
  id: 'hk',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/04/20210418_144143.jpg' },

{
  id: 'lon',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/04/Langhuset-arrangement-2-scaled-1.jpg' },

{
  id: 'nyc',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/04/20210418_143231.jpg' },

{
  id: 'tok',
  crossOrigin: true,
  type: createjs.Types.IMAGE,
  src: 'https://omgcommunity.no/wp-content/uploads/2021/04/20210418_153954.jpg' }]);



function handleProgress(event) {
  console.log(event.progress);
}

function handleLoad(event) {
  // console.log(event);
  var img = new Image();
  // img.crossOrigin = 'Anonymous';
  img.src = event.item.src;
  preloaded.appendChild(img);
  // var img = queue.getResult('image',true);
  // preloaded.appendChild(event.result);
}

function handleComplete() {
  document.querySelector('.main-container').classList.add('loaded');
  console.log('done');
}
//# sourceURL=pen.js
	
	
function refreshDiv(){
 var container = document.getElementById("main-container");
 var content = container.innerHTML;
//alert(content);
container.innerHTML= content;
}

setInterval(refreshDiv, 30000);

	
//function myFunction() {
//  var i = 0;
//  do {
//    i++;
//
//  }
//  while (i < 10);
//  
//}
//	
//myFunction();
</script>
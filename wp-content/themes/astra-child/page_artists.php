<?php
/**
 * The Template for displaying the artists page.
 *
 * @package WordPress
 * @subpackage pBone
 * Template name: Artists Page
 */?>
<?php if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header(); 
?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="inner-consciousness artists pb-4">
		<div class="ast-article-single">
			<?php the_content(); ?>
		</div>
	</div>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php else : ?>
<?php endif; ?>


<?php wp_reset_query(); ?>

<?php get_footer(); ?>
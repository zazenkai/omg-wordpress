<?php
/**
 * The Template for displaying how to play page parent page.
 *
 * @package WordPress
 * @subpackage pBone
 * Template name: How to Play parent Page 
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header(); 
?>



<div id="wrapper" class="HowToPlayPageParent mt-5 mb-5 bg-white container full-width ">
	<?php while ( have_posts() ) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	<?php endwhile; ?>
	<?php wp_reset_query(); ?>
    <?php
    global $post;
    $args = array(
        'parent'      => $post->ID,
        'post_type'   => 'page',
		'orderby' => 'menu_order',
		'order'   => 'DESC',
        'post_status' => 'publish'
    ); 
    $children = get_pages( $args );

    if ( ! empty( $children ) ) :
        ?>
        <div class="childcells d-flex pb-4 flex-wrap"> 
            <?php
            foreach ( $children as $post ) : setup_postdata( $post );
                ?>
                <div class="how-to-play-cells childcell col-md-3">
					<div class="card">
						<?php if ( has_post_thumbnail() ) : ?>
							<div class="thumbnail">
								<a class="image-link" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									<?php the_post_thumbnail('full', array('class' => 'img-fluid card-img-top')); ?>
								</a>
							</div>
						<?php endif; ?>
						<div class="card-body">
							<div class="myclasstitle"><h2 class="card-title"><?php the_title(); ?></h2></div>
							<span class="desc"><?php echo get_post_meta( get_the_ID(), 'desc', true ); ?></span>
							<div class="excerpt pb-2">
								<div><?php the_field('page_snippet'); ?></div>
							</div>
							<a class="btn btn-primary" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">Start now</a>
						</div>
					</div>
                </div>
                <?php
            endforeach;
            wp_reset_postdata();
            ?>
        </div>
    <?php endif; ?>
</div><!-- #wrapper -->


<?php get_footer(); ?> 
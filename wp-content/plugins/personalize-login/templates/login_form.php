<?php 
?>
<div class="login-form-container">
    <?php if ( $attributes['show_title'] ) : ?>
        <h2><?php _e( 'Sign In', 'personalize-login' ); ?></h2>
    <?php endif; ?>
     
	<!-- Show errors if there are any -->
	<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
		<?php foreach ( $attributes['errors'] as $error ) : ?>
			<p class="login-error">
				<?php echo $error; ?>
			</p>
		<?php endforeach; ?>
	<?php endif; ?>
	
    <?php
        my_wp_login_form(
            array(
                'label_username' => __( 'Email', 'personalize-login' ),
                'label_log_in' => __( 'Sign In', 'personalize-login' ),
                'redirect' => $attributes['redirect'],
            )
        );
    ?>
     

</div>
<?php
?>